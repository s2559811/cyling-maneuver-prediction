import torch
import torch.nn as nn
import torch.nn.functional as F  # for the activiation function

# LeNet-5 model on MNIST
# Layer C1 is a convoluational layer, obtains features, layer S2 downsamples this
# Layer C3 is another convolutional layer, combining the features from previous layers, layer S4 downsamples this
# Finally, a fully connected layers F5, F6 and OUTPUT, are a classifier that takes the final activation map, and classifies it into one of ten bins representing the 10 digits

class LeNet(nn.Module):  # inherits Module class
    def __init__(self):  # constructor
        super(LeNet, self).__init__()
        # 1 input image channel, 6 output channels
        # kernel
        self.conv1 = nn.Conv2d(1, 6, 3)
        self.conv2 = nn.Conv2d(6, 16, 3)
        # affine operation: y = Wx + b
        self.fc1 = nn.Linear(16 * 6 * 6, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        # Max pooling over a (2,2) window
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))

        x = F.max_pool2d(F.relu(self.conv2(x)), 2)
        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    def num_flat_features(self, x):
        size = x.size()[1:]  # all dimensions except batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features


# instantiate and run a sample input through it
net = LeNet()
print(net)

input = torch.rand(1, 1, 32, 32) #first digit represents batch dimension, batch size, amount of images in the batch
print(f'Image batch shape: {input.shape}')

output = net(input)
print(f'Raw output: {output} \n {output.shape}')
