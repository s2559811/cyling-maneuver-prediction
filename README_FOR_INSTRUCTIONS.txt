All files listed here contains the main code and data used for the report. The rest of the files in this repository were used for practice and designing the method.

'LSTM.ipynb' -> contains the main model used for evaluations in the report
'experiment_data' folder -> contains all experimental data obtained from 20 participants
'evaluations_plots.ipynb' -> contains the code used to generate the graphs in the results obtained from running the model
'evaluation_data' folder -> contains the data for the file above